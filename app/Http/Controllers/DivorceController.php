<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Divorce;
use Illuminate\Http\Request;

class DivorceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Husbands = User::where('Gender', 1)->get();
        $Wifes = User::where('Gender', 2)->get();
        return view('CreateDivorce', compact('Husbands', 'Wifes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
                'HusbandId' => 'integer|nullable',
                'WifeId' => 'integer|nullable',
                'DivorceDate' => 'date_format:Y-m-d',
        ]);
        Divorce::create([
                'HusbandId' => $request->input('HusbandId'),
                'WifeId' => $request->input('WifeId'),
                'DivorceDate' => $request->input('DivorceDate'),
        ]);
        session()->flash('CreateDivorce', 'Divorce created successfully.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Divorce  $divorce
     * @return \Illuminate\Http\Response
     */
    public function show(Divorce $divorce)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Divorce  $divorce
     * @return \Illuminate\Http\Response
     */
    public function edit(Divorce $divorce)
    {
        return view('EditDivorce');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Divorce  $divorce
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Divorce $divorce)
    {
        Divorce::where('Id', $divorce)->update([
                'HusbandId' => $request->input('HusbandId'),
                'WifeId' => $request->input('WifeId'),
                'DivorceDate' => $request->input('DivorceDate'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Divorce  $divorce
     * @return \Illuminate\Http\Response
     */
    public function destroy(Divorce $divorce)
    {
        //
    }

    /**
     * Show the form for searching divorces.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReportDivorce()
    {
        return view('ReportDivorce');
    }

    /**
     * Return the users for searching divorces.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReturnDivorce(Request $request)
    {
        $this->validate(request(), [
                'StartDate' => 'Required|date_format:Y-m-d',
                'EndDate' => 'Required|date_format:Y-m-d',
        ]);

        $Divorces = Divorce::where('DivorceDate', '>', $request->input('StartDate'))
            ->where('DivorceDate', '<', $request->input('EndDate'))
            ->leftjoin('users as u1', 'u1.id', '=', 'divorces.HusbandId')
            ->leftjoin('users as u2', 'u2.id', '=', 'divorces.WifeId')
            ->select('divorces.DivorceDate', 'u1.FirstName as HusbandFirstName', 'u1.LastName as HusbandLastName', 'u2.FirstName as WifeFirstName', 'u2.LastName as WifeLastName')
            ->get();
        return view('ReportDivorce', compact('Divorces'));
    }
}
