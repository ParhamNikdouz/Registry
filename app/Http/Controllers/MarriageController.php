<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Marriage;
use Illuminate\Http\Request;

class MarriageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Husbands = User::where('Gender', 1)->get();
        $Wifes = User::where('Gender', 2)->get();
        return view('CreateMarriage', compact('Husbands', 'Wifes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
                'HusbandId' => 'integer|nullable',
                'WifeId' => 'integer|nullable',
                'MarriageDate' => 'date_format:Y-m-d',
        ]);
        
        //Checking intimate
        // Check MotherId & FatherId if equal, they can't marriages
        // Check if exists in marriages table
        // Check uncles & aunt from MotherId & FotherId for example 
        //

        //Check does exist record in marriage table

        $HusbandId = $request->input('HusbandId');
        $WifeId = $request->input('WifeId');

        $Marriage = Marriage::where('HusbandId', $HusbandId)->where('WifeId', $WifeId)->count();
        if ($Marriage != 0)
        {
            session()->flash('ErrorMarriage', "This marriage is existed.");
            return redirect()->back();       
        }


        $HusbandFatherId = User::where('id', $HusbandId)->select('FatherId')->get();
        $WifeFatherId = User::where('id', $WifeId)->select('FatherId')->get();
        // OR
        $HusbandMotherId = User::where('id', $HusbandId)->select('MotherId')->get();
        $WifeMotherId = User::where('id', $WifeId)->select('MotherId')->get();

        if (($HusbandFatherId == $WifeFatherId) | ($HusbandMotherId == $WifeMotherId))
        {
            session()->flash('ErrorMarriage', "Marriage doesn't created. Because you are brother and sister!");
            return redirect()->back();
        }


        //Check marriage between boys & aunts(ameh)
        $HusbandFatherFatherId = User::where('id', $HusbandFatherId)->select('FatherId')->get();
        $WifeFatherId;
        if ($HusbandFatherFatherId == $WifeFatherId)
        {
            session()->flash('ErrorMarriage', "Marriage doesn't created. Because she is your aunt!");
            return redirect()->back();       
        }

        //Check marriage between boys & aunts(khaleh)
        $HusbandMotherMotherId = User::where('id', $HusbandMotherId)->select('MotherId')->get();
        $WifeMotherId;
        if ($HusbandMotherMotherId == $WifeMotherId)
        {
            session()->flash('ErrorMarriage', "Marriage doesn't created. Because she is your aunt!");
            return redirect()->back();       
        }

        //Check marriage between girls & uncle(amo)
        $WifeFatherFatherId = User::where('id', $WifeMotherId)->select('FatherId')->get();
        $HusbandFatherId;
        if ($WifeFatherFatherId == $HusbandFatherId)
        {
            session()->flash('ErrorMarriage', "Marriage doesn't created. Because he is your uncle!");
            return redirect()->back();       
        }

        //Check marriage between girls & uncle(daei)
        $WifeMotherMotherId = User::where('id', $WifeMotherId)->select('MotherId')->get();
        $HusbandMotherId;
        if ($WifeMotherMotherId == $HusbandMotherId)
        {
            session()->flash('ErrorMarriage', "Marriage doesn't created. Because he is your uncle!");
            return redirect()->back();       
        }

        Marriage::create([
                'HusbandId' => $request->input('HusbandId'),
                'WifeId' => $request->input('WifeId'),
                'MarriageDate' => $request->input('MarriageDate'),
        ]);
        session()->flash('CreateMarriage', 'Marriage created successfully.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marriage  $marriage
     * @return \Illuminate\Http\Response
     */
    public function show(Marriage $marriage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marriage  $marriage
     * @return \Illuminate\Http\Response
     */
    public function edit(Marriage $marriage)
    {
        return view('EditMarriage');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marriage  $marriage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marriage $marriage)
    {
        Marriage::where('Id', $marriage)->update([
                'HusbandId' => $request->input('HusbandId'),
                'WifeId' => $request->input('WifeId'),
                'MarriageDate' => $request->input('MarriageDate'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marriage  $marriage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marriage $marriage)
    {
        //
    }

    /**
     * Show the form for searching marriages.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReportMarriage()
    {
        return view('ReportMarriage');
    }

    /**
     * Return the users for searching marriages.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReturnMarriage(Request $request)
    {
        $this->validate(request(), [
                'StartDate' => 'Required|date_format:Y-m-d',
                'EndDate' => 'Required|date_format:Y-m-d',
        ]);

        $Marriages = Marriage::where('MarriageDate', '>', $request->input('StartDate'))
            ->where('MarriageDate', '<', $request->input('EndDate'))
            ->leftjoin('users as u1', 'u1.id', '=', 'marriages.HusbandId')
            ->leftjoin('users as u2', 'u2.id', '=', 'marriages.WifeId')
            ->select('marriages.MarriageDate', 'u1.FirstName as HusbandFirstName', 'u1.LastName as HusbandLastName', 'u2.FirstName as WifeFirstName', 'u2.LastName as WifeLastName')
            ->get();

        return view('ReportMarriage', compact('Marriages'));
    }
}
