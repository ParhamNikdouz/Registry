<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userInfo = User::where('users.id', $id)
            ->leftjoin('roles', 'users.RoleId', '=', 'roles.id')
            ->leftjoin('users as u1', 'u1.id', '=', 'users.FatherId')
            ->leftjoin('users as u2', 'u2.id', '=', 'users.MotherId')
            ->select('users.id', 'users.FirstName', 'users.LastName',
                    'u1.FirstName as FatherFirstName', 'u1.LastName as FatherLastName',
                    'u2.FirstName as MotherFirstName', 'u2.LastName as MotherLastName',
                    'users.Gender', 'users.NationalCode',
                    'users.BirthDate', 'users.DeathDate',
                    'users.FatherId', 'users.MotherId',
                    'roles.RoleName', 'users.username',
                    'users.Password', 'users.ProfileImage')
            ->get();
        return view('ShowUser', compact('userInfo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Fothers = User::where('Gender', 1)->get();
        $Mothers= User::where('Gender', 2)->get();
        $Roles = Role::all();
        $userInfo = User::where('id', $id)->get();
        return view('EditUser', compact('Fothers', 'Mothers', 'Roles', 'userInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
                'FirstName' => 'Required|String|max:255',
                'LastName' => 'Required|String|max:255',
                'Username' => 'Required|String|max:255',
                // 'password' => 'Required|String|confirmed|min:6',
                'FatherId' => 'integer|nullable',
                'MotherId' => 'integer|nullable',
                'NationalCode' => 'Required|String',
                'BirthDate' => 'date_format:Y-m-d',
                'DeathDate' => 'date_format:Y-m-d|nullable',
                'Gender' => 'Required|integer',
                'RoleId' => 'Required|integer',
                // 'ProfileImage' => 'mimes:jpeg,png,jpg,gif,svg,jpg|nullable',
        ]);

        // Save Images in directory
        if($request->hasFile('ProfileImage')) 
        {
            $image = $request->file('ProfileImage');
            $destinationPath = public_path() . '/uploads';
            $fileName = time(). '.' . $image->getClientOriginalExtension();

            $image->move($destinationPath, $fileName);
        }
        else
        {
            $userInfo = User::where('id', $id)
                ->select('users.ProfileImage')
                ->get();
            $fileName = $userInfo[0]->ProfileImage;
        }

        User::where('id', $id)->update([
                'FirstName' => $request->input('FirstName'),
                'LastName' => $request->input('LastName'),
                'username' => $request->input('Username'),
                // 'password' => $request->input('password'),
                'FatherId' => $request->input('FatherId'),
                'MotherId' => $request->input('MotherId'),
                'NationalCode' => $request->input('NationalCode'),
                'BirthDate' => $request->input('BirthDate'),
                'DeathDate' => $request->input('DeathDate'),
                'Gender' => $request->input('Gender'),
                'RoleId' => $request->input('RoleId'),
                'ProfileImage' => $fileName,
        ]);
        session()->flash('EditUser', 'User updated successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for searching users.
     *
     * @return \Illuminate\Http\Response
     */
    public function SearchUser()
    {
        return view('SearchUser');
    }

    /**
     * Show the routes for reporting users.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReturnUser(Request $request)
    {
        $this->validate(request(), [
                'FirstName' => 'nullable|String',
                'LastName' => 'nullable|String',
                'NationalCode' => 'nullable|String',
        ]);

        $Users = array();
        if (Auth::user()->RoleId == 1)
        {
            if($request->input('FirstName') !== null)
            {
                $Users = User::where('FirstName', $request->input('FirstName'))
                    ->where('RoleId', '<>', 1)
                    ->get();
            }

            if ($request->input('LastName') !== null)
            {
                $Users = User::where('LastName', $request->input('LastName'))
                    ->where('RoleId', '<>', 1)
                    ->get();
            }

            if ($request->input('NationalCode') !== null)
            {
                $Users = User::where('NationalCode', $request->input('NationalCode'))
                    ->where('RoleId', '<>', 1)
                    ->get();
            }
            return view('SearchUser', compact('Users'));
        }
        else
        {
             if($request->input('FirstName') !== null)
            {
                $Users = User::where('FirstName', $request->input('FirstName'))
                    ->where('RoleId', '<>', 2)
                    ->get();
            }

            if ($request->input('LastName') !== null)
            {
                $Users = User::where('LastName', $request->input('LastName'))
                    ->where('RoleId', '<>', 2)
                    ->get();
            }

            if ($request->input('NationalCode') !== null)
            {
                $Users = User::where('NationalCode', $request->input('NationalCode'))
                    ->where('RoleId', '<>', 2)
                    ->get();
            }
            return view('SearchUser', compact('Users'));       
        }


    }

    public function ReportUser() 
    {
        return view('ReportUser');
    }
}
