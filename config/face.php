<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Face provider
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the face providers below you wish
    | to use as your default.
    |
    */
    'default_provider' => 'face_plus_plus',

    /*
    |--------------------------------------------------------------------------
    | Face Providers
    |--------------------------------------------------------------------------
    |
    */
    'providers' => [
        /*
        | Face++ Provider
        | faceplusplus.com
        */
        'face_plus_plus' => [
            'api_key' => env('po09IaC-Hgr7bc-4CsCVPM-F9AIl9ocT'),
            'api_secret' => env('CgBqDq6pU_qXyDvyeeCdnZIgoBf0EIEY'),
        ],
    ],
];
