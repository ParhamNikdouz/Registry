@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (Session::has('CreateMarriage'))
                <div class="alert alert-success">
                    <strong>{{ Session::get('CreateMarriage') }}</strong>
                </div>
            @endif
            @if (Session::has('ErrorMarriage'))
                <div class="alert alert-danger">
                    <strong>{{ Session::get('ErrorMarriage') }}</strong>
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Create Marriage') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('StoreMarriage') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="HusbandId" class="col-md-4 col-form-label text-md-right">{{ __('Husband') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('HusbandId') ? ' is-invalid' : '' }}" id="HusbandId" name="HusbandId" value="{{ old('HusbandId') }}" required>
                                    <option value=""></option> 
                                    @foreach ($Husbands as $Husband)
                                        <option value="{{ $Husband->id }}">{{ $Husband->FirstName }} {{ $Husband->LastName }}</option> 
                                    @endforeach
                                </select>

                                @if ($errors->has('HusbandId'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('HusbandId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="WifeId" class="col-md-4 col-form-label text-md-right">{{ __('Wife') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('WifeId') ? ' is-invalid' : '' }}" id="WifeId" name="WifeId" value="{{ old('WifeId') }}" required>
                                    <option value=""></option> 
                                    @foreach ($Wifes as $Wife)
                                        <option value="{{ $Wife->id }}">{{ $Wife->FirstName }} {{ $Wife->LastName }}</option> 
                                    @endforeach
                                </select>

                                @if ($errors->has('WifeId'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('WifeId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="MarriageDate" class="col-md-4 col-form-label text-md-right">{{ __('MarriageDate') }}</label>

                            <div class="col-md-6">
                                <input id="MarriageDate" type="date" class="form-control{{ $errors->has('MarriageDate') ? ' is-invalid' : '' }}" name="MarriageDate" value="{{ old('MarriageDate') }}" required>

                                @if ($errors->has('MarriageDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('MarriageDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
