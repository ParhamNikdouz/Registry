@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            {{-- dd($errors->all()) --}}
            @if (Session::has('EditUser'))
                <div class="alert alert-success">
                    <strong>{{ Session::get('EditUser') }}</strong>
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Edit User') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('UpdateUser', ['user' => $userInfo[0]->id]) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="FirstName" class="col-md-4 col-form-label text-md-right">{{ __('FirstName') }}</label>

                            <div class="col-md-6">
                                <input id="FirstName" type="text" class="form-control{{ $errors->has('FirstName') ? ' is-invalid' : '' }}" name="FirstName" value="{{ $userInfo[0]->FirstName }}" required autofocus>

                                @if ($errors->has('FirstName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('FirstName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="LastName" class="col-md-4 col-form-label text-md-right">{{ __('LastName') }}</label>

                            <div class="col-md-6">
                                <input id="LastName" type="text" class="form-control{{ $errors->has('LastName') ? ' is-invalid' : '' }}" name="LastName" value="{{ $userInfo[0]->LastName }}" required>

                                @if ($errors->has('LastName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('LastName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="NationalCode" class="col-md-4 col-form-label text-md-right">{{ __('NationalCode') }}</label>

                            <div class="col-md-6">
                                <input id="NationalCode" type="text" class="form-control{{ $errors->has('NationalCode') ? ' is-invalid' : '' }}" name="NationalCode" value="{{ $userInfo[0]->NationalCode }}" required>

                                @if ($errors->has('NationalCode'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('NationalCode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="BirthDate" class="col-md-4 col-form-label text-md-right">{{ __('BirthDate') }}</label>

                            <div class="col-md-6">
                                <input id="BirthDate" type="date" class="form-control{{ $errors->has('BirthDate') ? ' is-invalid' : '' }}" name="BirthDate" value="{{ $userInfo[0]->BirthDate }}" required>

                                @if ($errors->has('BirthDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('BirthDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="DeathDate" class="col-md-4 col-form-label text-md-right">{{ __('DeathDate') }}</label>

                            <div class="col-md-6">
                                <input id="DeathDate" type="date" class="form-control{{ $errors->has('DeathDate') ? ' is-invalid' : '' }}" name="DeathDate" value="{{ $userInfo[0]->DeathDate }}">

                                @if ($errors->has('DeathDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('DeathDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('Gender') ? ' is-invalid' : '' }}" id="Gender" name="Gender" value="{{ old('Gender') }}">
                                    <option value=""></option>
                                    <option value="1" {{ $userInfo[0]->Gender == 1 ? 'selected' : '' }} >Male</option>
                                    <option value="2" {{ $userInfo[0]->Gender == 2 ? 'selected' : '' }} >Female</option>
                                </select>

                                @if ($errors->has('Gender'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="FatherId" class="col-md-4 col-form-label text-md-right">{{ __('Father') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('FatherId') ? ' is-invalid' : '' }}" id="FatherId" name="FatherId" value="{{ $userInfo[0]->FatherId }}">
                                    <option value=""></option> 
                                    @foreach ($Fothers as $Fother)
                                        <option value="{{ $Fother->id }}" {{ $Fother->id == $userInfo[0]->FatherId ? 'selected' : '' }}>{{ $Fother->FirstName }} {{ $Fother->LastName }}</option> 
                                    @endforeach
                                </select>

                                @if ($errors->has('FatherId'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('FatherId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="MotherId" class="col-md-4 col-form-label text-md-right">{{ __('Mother') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('MotherId') ? ' is-invalid' : '' }}" id="MotherId" name="MotherId" value="{{ $userInfo[0]->MotherId }}">
                                    <option value=""></option>
                                    @foreach ($Mothers as $Mother)
                                        <option value="{{ $Mother->id }}" {{ $Mother->id == $userInfo[0]->MotherId ? 'selected' : '' }}>{{ $Mother->FirstName }} {{ $Mother->LastName }}</option> 
                                    @endforeach
                                </select>

                                @if ($errors->has('MotherId'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('MotherId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="RoleId" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('RoleId') ? ' is-invalid' : '' }}" id="RoleId" name="RoleId" value="{{ $userInfo[0]->RoleId }}" required>
                                    <option value=""></option> 
                                    @if (Auth::user()->RoleId == 1)
                                        <option value="2" {{ 2 == $userInfo[0]->RoleId ? 'selected' : '' }}>Employee</option> 
                                        <option value="3" {{ 3 == $userInfo[0]->RoleId ? 'selected' : '' }}>User</option> 
                                    @endif
                                    @if (Auth::user()->RoleId == 2)
                                        <option value="3" {{ 3 == $userInfo[0]->RoleId ? 'selected' : '' }}>User</option> 
                                    @endif
                                </select>

                                @if ($errors->has('RoleId'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('RoleId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="Username" type="text" class="form-control{{ $errors->has('Username') ? ' is-invalid' : '' }}" name="Username" value="{{ $userInfo[0]->username }}" required>

                                @if ($errors->has('Username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!--<div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ $userInfo[0]->Password }}" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{ $userInfo[0]->Password }}" required>
                            </div>
                        </div>-->

                        <div class="form-group row">
                            <label for="ProfileImage" class="col-md-4 col-form-label text-md-right">{{ __('ProfileImage') }}</label>

                            <div class="col-md-6">
                                <input id="ProfileImage" style="padding:0; border:0;" type="file" class="form-control{{ $errors->has('ProfileImage') ? ' is-invalid' : '' }}" name="ProfileImage" value="{{ $userInfo[0]->ProfileImage }}" accept="image/*">

                                @if ($errors->has('ProfileImage'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ProfileImage') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
