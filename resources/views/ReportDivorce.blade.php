@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Search Divorces') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('ReturnDivorce') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="StartDate" class="col-md-4 col-form-label text-md-right">{{ __('StartDate') }}</label>

                            <div class="col-md-6">
                                <input id="StartDate" type="date" class="form-control{{ $errors->has('StartDate') ? ' is-invalid' : '' }}" name="StartDate" value="{{ old('StartDate') }}" required>

                                @if ($errors->has('StartDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('StartDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="EndDate" class="col-md-4 col-form-label text-md-right">{{ __('EndDate') }}</label>

                            <div class="col-md-6">
                                <input id="EndDate" type="date" class="form-control{{ $errors->has('EndDate') ? ' is-invalid' : '' }}" name="EndDate" value="{{ old('EndDate') }}" required>

                                @if ($errors->has('EndDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('EndDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Search') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @isset($Divorces)
                @if (count($Divorces) == 0)
                    <br>
                    <div class="alert alert-danger">
                        <strong>Marriages not found!</strong>
                    </div>
                @else
                <br>
                <ul class="list-group">
                    @foreach ($Divorces as $Divorce)
                        <li class="list-group-item list-group-item-action">
                            {{ $Divorce->HusbandFirstName}} {{ $Divorce->HusbandLastName}} //
                            {{ $Divorce->WifeFirstName}} {{ $Divorce->WifeLastName}} //
                            {{ $Divorce->MarriageDate}}
                        </li>
                    @endforeach
                </ul>
                @endif
            @endisset

        </div>
    </div>
</div>
@endsection
