@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <ul class="list-group">
                <li class="list-group-item list-group-item-info">Report Users</li>
                <li class="list-group-item list-group-item-action text-center"><a href="{{ route('ReportMarriage') }}">ReportMarriage</a></li>
                <li class="list-group-item list-group-item-action text-center"><a href="{{ route('ReportDivorce') }}">ReportDivorce</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection
