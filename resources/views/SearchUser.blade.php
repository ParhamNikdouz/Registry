@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Search By...') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('ReturnUser') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="FirstName" class="col-md-4 col-form-label text-md-right">{{ __('FirstName') }}</label>

                            <div class="col-md-6">
                                <input id="FirstName" type="text" class="form-control{{ $errors->has('FirstName') ? ' is-invalid' : '' }}" name="FirstName" value="{{ old('FirstName') }}" autofocus>

                                @if ($errors->has('FirstName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('FirstName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="LastName" class="col-md-4 col-form-label text-md-right">{{ __('LastName') }}</label>

                            <div class="col-md-6">
                                <input id="LastName" type="text" class="form-control{{ $errors->has('LastName') ? ' is-invalid' : '' }}" name="LastName" value="{{ old('LastName') }}" >

                                @if ($errors->has('LastName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('LastName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="NationalCode" class="col-md-4 col-form-label text-md-right">{{ __('NationalCode') }}</label>

                            <div class="col-md-6">
                                <input id="NationalCode" type="text" class="form-control{{ $errors->has('NationalCode') ? ' is-invalid' : '' }}" name="NationalCode" value="{{ old('NationalCode') }}" >

                                @if ($errors->has('NationalCode'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('NationalCode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Search') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @isset($Users)
                @if (count($Users) == 0)
                    <br>
                    <div class="alert alert-danger">
                        <strong>User not found!</strong>
                    </div>
                @else
                <br>
                <ul class="list-group">
                    @foreach ($Users as $User)
                        <li class="list-group-item list-group-item-action"><a href="{{ route('ShowUser', ['user' => $User->id]) }}">{{ $User->FirstName }} {{ $User->LastName }}</a></li>
                    @endforeach
                </ul>
                @endif
            @endisset
        </div>
        </div>
    </div>
</div>
@endsection
