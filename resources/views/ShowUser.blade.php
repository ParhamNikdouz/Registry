@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <ul class="list-group">
                <li class="list-group-item list-group-item-info">User Info</li> 
                <li class="list-group-item list-group-item-action text-center"><img src="{{ URL::to('/') . "/uploads/" . $userInfo[0]->ProfileImage }}" width="150" height="150" style="border-radius:100%;"></li>
                <li class="list-group-item list-group-item-action">FirstName: {{ $userInfo[0]->FirstName }}</li>
                <li class="list-group-item list-group-item-action">LastName: {{ $userInfo[0]->LastName }}</li>
                <li class="list-group-item list-group-item-action">NationalCode: {{ $userInfo[0]->NationalCode }}</li>
                <li class="list-group-item list-group-item-action">BirthDate: {{ $userInfo[0]->BirthDate }}</li>
                <li class="list-group-item list-group-item-action">DeathDate: {{ $userInfo[0]->DeathDate }}</li>
                <li class="list-group-item list-group-item-action">Gender:
                    @if ($userInfo[0]->Gender == 1)
                        <span>Male</span>
                    @elseif ($userInfo[0]->Gender == 2)
                        <span>Female</span>
                    @else
                    @endif
                </li>
                <li class="list-group-item list-group-item-action">Father: {{ $userInfo[0]->FatherFirstName }} {{ $userInfo[0]->FatherLastName }}</li>
                <li class="list-group-item list-group-item-action">Mother: {{ $userInfo[0]->MotherFirstName }} {{ $userInfo[0]->MotherLastName }}</li>
                <li class="list-group-item list-group-item-action">Role: {{ $userInfo[0]->RoleName }}</li>
                <li class="list-group-item list-group-item-action">Username: {{ $userInfo[0]->username }}</li>
                <!--<li class="list-group-item list-group-item-action">Password: {{ $userInfo[0]->Password }}</li>-->

            </ul>
        </div>

        @if ( Auth::user()->RoleId  == 1 | Auth::user()->RoleId  == 2)
        <div class="col-md-8">
            <br>
            <a class="btn btn-primary" href="{{ route('EditUser', ['user' => $userInfo[0]->id]) }}">
                {{ __('Edit User') }}
            </a>
        </div>
        @endif
    </div>
</div>
@endsection
