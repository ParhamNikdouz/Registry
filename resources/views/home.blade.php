@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <ul class="list-group">

                @if ( Auth::user()->RoleId  == 1 | Auth::user()->RoleId  == 2)
                    <li class="list-group-item list-group-item-info">Dashboard</li>
                    <li class="list-group-item list-group-item-action text-center"><a href="{{ route('register') }}">Add User</a></li>
                    <li class="list-group-item list-group-item-action text-center"><a href="{{ route('SearchUser') }}">Search User</a></li>
                    <li class="list-group-item list-group-item-action text-center"><a href="{{ route('CreateMarriage') }}">Add Marriage</a></li>
                    <li class="list-group-item list-group-item-action text-center"><a href="{{ route('CreateDivorce') }}">Add Divorce</a></li>
                    <li class="list-group-item list-group-item-action text-center"><a href="{{ route('ReportUser') }}">Report Marriages/Divorces</a></li>
                @else
                    <script type="text/javascript">
                        window.location = "{{ url('/show/user/'.Auth::user()->id) }}";
                    </script>
                @endif

            </ul>
        </div>
    </div>
</div>
@endsection
