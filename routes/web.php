<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

//User CRUD
Route::get('/add/user', 'UserController@create')->name('CreateUser')->middleware('auth');
Route::post('/add/user', 'UserController@store')->name('StoreUser')->middleware('auth');
Route::get('/edit/user/{id}', 'UserController@edit')->name('EditUser')->middleware('auth');
Route::post('/edit/user/{id}', 'UserController@update')->name('UpdateUser')->middleware('auth');

//Search User
Route::get('/search/user', 'UserController@SearchUser')->name('SearchUser')->middleware('auth');
Route::post('/search/user', 'UserController@ReturnUser')->name('ReturnUser')->middleware('auth');
Route::get('/show/user/{id}', 'UserController@show')->name('ShowUser')->middleware('auth');

//Report Page
Route::get('/report/user', 'UserController@ReportUser')->name('ReportUser')->middleware('auth');

//Report Marriages
Route::get('/add/marriage', 'MarriageController@create')->name('CreateMarriage')->middleware('auth');
Route::post('/add/marriage', 'MarriageController@store')->name('StoreMarriage')->middleware('auth');
// Route::get('/edit/marriage/{id}', 'MarriageController@edit')->name('EditMarriage');//->middleware('auth');
// Route::post('/edit/marriage', 'MarriageController@update')->name('UpdateMarriage');//->middleware('auth');
Route::get('/report/marriage', 'MarriageController@ReportMarriage')->name('ReportMarriage')->middleware('auth');
Route::post('/report/marriage', 'MarriageController@ReturnMarriage')->name('ReturnMarriage')->middleware('auth');

//Report Divorces
Route::get('/add/divorce', 'DivorceController@create')->name('CreateDivorce') ->middleware('auth');
Route::post('/add/divorce', 'DivorceController@store')->name('StoreDivorce')->middleware('auth');
// Route::get('/edit/divorce/{id}', 'DivorceController@edit')->name('EditDivorce');//->middleware('auth');
// Route::post('/edit/divorce', 'DivorceController@update')->name('UpdateDivorce');//->middleware('auth');
Route::get('/report/divorce', 'DivorceController@ReportDivorce')->name('ReportDivorce')->middleware('auth');
Route::post('/report/divorce', 'DivorceController@ReturnDivorce')->name('ReturnDivorce')->middleware('auth');
